FROM ruby

RUN apt -y update
RUN apt -y install postgresql postgresql-contrib libpq-dev
RUN apt -y install vim

RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get install -y nodejs
RUN npm install --global yarn

RUN npm install -g heroku

COPY gems.txt /user_dev/gems.txt
COPY install_gems.sh /user_dev/install_gems.sh
RUN cd /user_dev && ./install_gems.sh

COPY entrypoint.sh /user_dev/entrypoint.sh
ENTRYPOINT ["/user_dev/entrypoint.sh"]
